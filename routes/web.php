<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
Route::group(['middleware' => 'myfy'], function () {
	Route::get('/admin/dashboard', 'Admin\DashboardController@index')->name('dashboard');
	Route::get('/admin/page/create', 'Admin\PageController@create')->name('page.create');
	Route::post('/admin/pages', 'Admin\PageController@store')->name('page.store');
	Route::get('/admin/page/edit/{id}', 'Admin\PageController@edit')->name('page.edit');
	Route::put('/admin/pages/{id}', 'Admin\PageController@update')->name('page.update');
	Route::get('/admin/pages', 'Admin\PageController@index')->name('page.index');
	Route::get('/admin/pages/{id}', 'Admin\PageController@show')->name('page.show');
	Route::delete('/admin/pages/{id}', 'Admin\PageController@destroy')->name('page.destroy');
});

Route::get('/{slug}', 'Site\PageController@show')->name('page');
Route::get('/', 'Site\HomeController@index')->name('home');
