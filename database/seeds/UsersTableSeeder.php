<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->insert([
			[
	            'name' => 'admin',
	            'email' => 'admin@promo-site.example',
	            'password' => '$2y$10$EU41xQ2/TEzcAcfWAjLLKe16y7yuMbQiv0fAkL0sVpdKmRxfEq60y',
	        ]
	    ]);
    }
}
