@extends('admin.layout')

@section('title', $page->name)

@section('sidebar')
    @parent

    <p>Это дополнение к основной боковой панели.</p>
@endsection

@section('content')
<h1>{{ $page->name }}</h1>
{!! Form::open(['url' => '/admin/pages/' . $page->id, 'method' => 'delete']) !!}
		{{ Form::submit('Delete') }}
{!! Form::close() !!}

@endsection
