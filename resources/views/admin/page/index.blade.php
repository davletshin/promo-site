@extends('admin.layout')

@section('title', 'Страницы')

@section('sidebar')
    @parent

    <p>Это дополнение к основной боковой панели.</p>
@endsection

@section('content')
<h1>Страницы</h1>
<ul>
	@foreach($pages as $page)
		@include('admin.list-item', ['item' => $page])
	@endforeach
</ul>
{{ $pages->links() }}
@endsection
