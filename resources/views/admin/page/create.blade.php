@extends('admin.layout')

@section('title', 'Страницы')

@section('sidebar')
    @parent

    <p>Это дополнение к основной боковой панели.</p>
@endsection

@section('content')
{!! Form::open(['url' => '/admin/pages', 'method' => 'post']) !!}
	@include('admin.page.form')
{!! Form::close() !!}@if ($errors->any())
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@endsection
